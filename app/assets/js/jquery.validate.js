var validator = $("#form1");

$("#form1").validate({
  onfocusout: false,
  invalidHandler: function(form, validator) {
    var errors = validator.numberOfInvalids();
    if (errors) {
      validator.errorList[0].element.focus();
    }
  },
  rules: {
    firstName1: {
      required: true
    },
    secondName1: {
      required: true
    },
    role: {
      required: true
    },
    contactnumber: {
      required: true,
      phoneUK: true
    },
    emailaddress: {
      required: true,
      email: true
    },
    activitytitle: {
      required: true
    },
    goverance: {
      required: true
    },
    date: {
      required: true,
      date: true
    }
  },
  messages: {
    firstName1: "Please specify your first name",
    secondName1: "Please specify your second name",
    role: "Please specify your role",
    contactnumber: "Please enter valid contact number",
    emailaddress: "Please enter valid email address",
    activitytitle: "Please enter an appropriate title",
    goverance: "Please select Yes or No",
    date: "Please specify a correct date"
  }
});

$("#letsgetstarted").click(function() {
  if($("#form1").valid()) {
        event.preventDefault();
        $letsgetstarted.show();
  }
});

var validator = $("#form2");

$("#form2").validate({
  onfocusout: false,
  invalidHandler: function(form, validator) {
    var errors = validator.numberOfInvalids();
    if (errors) {
      validator.errorList[0].element.focus();
    }
  },
  rules: {
    copy: {
        required: true,
        minlength: 8
    },
    searchterms: {
        required: true
    }
    
  },
  messages: {
    copy: "Please insert or attach copy",
    searchterms: "Please define search terms"
  }
});

$("#copychange").click(function() {
  if($("#form2").valid()) {
        event.preventDefault();
        $copychange.show();
  }
});

