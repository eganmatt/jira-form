(function() {
  var formItems = document.querySelectorAll('.js-boolean-has-message');

  var setMessageState = function(messages, isYes) {
    messages.yes.style.display = isYes ? 'block' : 'none';
    messages.no.style.display = isYes ? 'none' : 'block';
  }

  var initMessages = function(element) {
    var radios = element.querySelectorAll('input[type="radio"]');

    var messages = {
      yes: element.querySelector('.js-boolean-has-message-yes'),
      no: element.querySelector('.js-boolean-has-message-no')
    };

    for (var i = 0; radios.length > i; i++) {
      if (radios[i].checked) {
        setMessageState(messages, radios[i].value === "yes");
      }
    }

    element.querySelector('[value="yes"]').addEventListener('click', function() {
      setMessageState(messages, true);
    });

    element.querySelector('[value="no"]').addEventListener('click', function() {
      setMessageState(messages);
    });
  };

  for (var i = 0; formItems.length > i; i++) {
    initMessages(formItems[i]);
  }
})();
