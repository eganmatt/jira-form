$("#postsubmit").click(function() {
  var ticket = {
    firstName: $("#firstName1").val(),
    secondName: $("#secondName1").val(),
    role: $("#role").val(),
    contactNumber: $("#contactnumber").val(),
    emailAddress: $("#emailaddress").val(),
    governanceyes: $("#governanceyes").val(),
    governanceno: $("#governanceno").val(),
    activitytitle: $("#activitytitle").val(),
    dateday: $("#date-day").val(),
    datemonth: $("#date-month").val(),
    dateyear: $("#date-year").val(),
     criticalyes: $("#critical-yes").val(),
     criticalno: $("#critical-no").val(),
     whatTime: $("#what-time").val(),
     driveDate: document.getElementById("drive-date").value,
  //   other: $("#other").val(),
  //   location: $("#location").val(),
  //   nonnegotiableyes: $("#non-negotiable-yes").val(),
  //   nonnegotiableno: $("#non-negotiable-no").val(),
     searchterms: $("#search-terms").val(),
  //   assetsyes: $("#assets-yes").val(),
  //   assetsno: $("#assets-no").val(),
  //   exisitingassetsyes: $("#exisiting-assets-yes").val(),
  //   exisitingassetsno: $("#exisiting-assets-no").val(),
  //   assetslocationyes: $("#assets-location-yes").val(),
  //   assetslocationno: $("#assets-location-no").val(),
  //   //attach asset//
  //   pdfcode: $("#pdf-code").val(),
  //   //attach asset//
  //   datedayasset: $("#date-day-asset").val(),
  //   datemonthasset: $("#date-month-asset").val(),
  //   dateyearasset: $("#date-year-asset").val(),
  //   ratesyes: $("#rates-yes").val(),
  //   ratesno: $("#rates-no").val(),
  //   datedayrates: $("#date-rate-rates").val(),
  //   datemonthrates: $("#date-month-rates").val(),
  //   dateyearrates: $("#date-year-rates").val(),
  //   ratechange: $("#rate-change").val(),
  //   rateallchange: $("#rate-all-change").val(),
  //   //attach asset//
  //   bannerobjectiveyes: $("#banner-objective-yes").val(),
  //   bannerobjectiveno: $("#banner-objective-no").val(),
  //   domainobjective: $("#domain-objective").val(),
  //   linkbanner: $("#link-banner").val(),
  //   bannercopy: $("#banner-copy").val(),
  //   //attach-asset//
  //   aspirationalurl: $("#aspirational-url").val(),
  //   //attach-asset//
  //   linksonpageyes: $("#links-on-page-yes").val(),
  //   linksonpageno: $("#links-on-page-no").val(),
  //   pagelinkyes: $("#page-link-yes").val(),
  //   pagelinkno: $("#page-link-no").val(),
  //   pagefromurl: $("#page-from-url").val(),
  //   sitemapnewyes: $("#sitemap-new-yes").val(),
  //   sitemapnewno: $("#sitemap-new-no").val(),
  //   setUpyes: $("#setUp-yes").val(),
  //   setUpno: $("#setUp-no").val(),
  //   smeglibraries: $("#smeg-libraries").val(),
  //   keywords: $("#key-words").val(),
  //   proposedcopy: $("#proposed-copy").val(),
  //   //attach asset//
  //   sitescrape: $("#site-scrape").val(),
  //   scrapedomain: document.getElementById("scrape-domain").value,
  //   sitescrapesearch: $("#site-scrape-search").val(),
  //   delegatedasset: $("#delegated-asset").val(),
  //   deletedasset: $("#deleted-asset").val(),
  //   definedsearchterm: $("#defined-saerch-term").val(),
  //   otherrequestdetails: $("#other-request-details").val(),
  //   locationrequestdetails: $("#location-request-details").val(),
  //   otherrequestsearchterms: $("#other-request-search-terms").val(),
     achievedetails: $("#achieve-details").val(),
  //   successdetails: $("#success-details").val(),
  //   budgetdetails: $("#budget-details").val(),
  //   currentlyknowdetails: $("#currently-know-details").val()
  };

  
  var contentUpdateBrief = "{panel:title=Content Update|borderColor=#002663|titleBGColor=#002663|bgColor=#f2f7fb} " +" \\" +
"*Content to Update: (Mandatory)* " + " \\" + 
"Updated copy... or if attached: See attachment (name of attachment)" + " \\" + 
"h4. Pages Affected:" + " \\" + 
" \\" + 
$("#achieve-details").val() + " \\" + 
" \\" + 
"h4. {color:#333333}*Site Scrape Search Terms:*{color}" + " \\" + 
" \\" + 
$("#search-terms").val() + " \\" + 
" \\" + 
"Additional information..." + " \\" + 
"h4. Systems Affected -" + " \\" + 
"||System||Affected||" + " \\" + 
"|Brochareware (Public Website)|Yes|" + " \\" + 
"|Application Form|Yes / No|" + " \\" + 
"|Online Banking|Yes / No|" + " \\" + 
"|Mobile App|Yes / No|" + " \\" + 
"h4. Risk of Doing Nothing:" + " \\" + 
" \\" + 
"Risk of doing nothing..." + " \\" + 
"h4. Delivery Date: "+ $("#date-day").val() + "/" + $("#date-month").val() + "/" + $("#date-year").val() + " \\" + 
" \\" + 
"Due Date - "+ $("#date-day").val() + "/" + $("#date-month").val() + "/" + $("#date-year").val() + " \\" + 
" \\" + 
"Critical Live Time -"+ $("#what-time").val() + " \\" + 
" \\" + 
"Driven by -" + document.getElementById("drive-date").value  + " \\" + 
"{panel}" + " \\" +
"{panel:title=Governance and Stakeholders|borderColor=#002663|titleBGColor=#002663|bgColor=#f2f7fb}" + " \\" + 
"h4. Governance:" + " \\" + 
" \\" + 
"Has this request been through the appropriate governance and signoff prior to submission? -" + $("#governanceyes").val() + " \\" + 
"h4. Stakeholders:" + " \\" + 
"||Name||Role||Contact Number||Email||" + " \\" +  
"|Submitter details: First Name + Second Name|Role|Contact Number|Email Address|" + " \\" + 
"|" + $("#firstName1").val() + " " +$("#secondName1").val() +"|"+ $("#role").val() +"|"+ $("#contactnumber").val() +"|"+ $("#emailaddress").val() +"|" + " \\" + 
"|Approver 1: First Name + Second Name|Role|Contact Number|Email Address|" + " \\" + 
"|" + $("#firstName1").val() + " " +$("#secondName1").val() +"|"+ $("#role").val() +"|"+ $("#contactnumber").val() +"|"+ $("#emailaddress").val() +"|" + " \\" + 
"|Approver 2: First Name + Second Name|Role|Contact Number|Email Address|" + " \\" + 
"|" + $("#firstName1").val() + " " +$("#secondName1").val() +"|"+ $("#role").val() +"|"+ $("#contactnumber").val() +"|"+ $("#emailaddress").val() +"|" + " \\" + 
"{panel}";


//   var digitalBrief = "{panel:title=Brief|titleBGColor=#f5f5f5|bgColor=#f2f7fb}\n\
//  h4. Summary of Change -" + $("#activitytitle").val() + "\n\
//  Due Date: " + $("#date-day").val() + "/" + $("#date-month").val() + "/" + $("#date-year").val()+ "\n\
//  Time of day: \n\
//  critical : yes\n\
//  what drives the delivery date: \n\
//  governance process applied:\n\
// \n\
// h4. Products Affected -\n\
// ||Product (Please State)||Retail / SME||\n\
// |Customer Service|Retail|\n\
// |Current Accounts|Retail|\n\
// \n\
// h4. Systems Affected -\n\
// ||System||Affected||\n\
// |Brochareware (Public Website)|Yes / No|\n\
// |Application Form|Yes / No|\n\
// |Online Banking|Yes / No|\n\
// |Mobile App|Yes / No|\n\
// h4. Pages Affected -\n\
// \n\
// \n\
// {panel}\n\
// {panel:title=Target KPIs|titleBGColor=#f5f5f5|bgColor=#f2f7fb}\n\
// h4. What Metrics drive this change?\n\
// \n\
// \n\
// h4. Risk of Doing Nothing -\n\
// \n\
// \n\
// {panel}\n\
// {panel:title=Submitter details|titleBGColor=#f5f5f5|bgColor=#f2f7fb}\n\
// |*Name*|*Role*|*Contact number*|*Email*|\n\
// |" + $("#firstName1").val() + " " +$("#secondName1").val() +"|"+ $("#role").val() +"|"+ $("#contactnumber").val() +"|"+ $("#emailaddress").val() +"|" + "\n\
// |*Alternative Contact*|*Role*|*Contact number*|*Email*|\n\
// | | | | |\n\
// {panel}";

//MATTTTTTTTT
//YOU NEED TO TALK TO TOM CRYAN about the jira tickets you need to mimic. 
// I created a confluence doc https://adc-confluence.uk.capgemini.com/display/DTO/Project%3A+Jira+form+and+Jira+form+server
// the bottom tickets have some of the info you need.
//I didnt get a chance to implement the file attachment procedure
// for that you first need to create the ticket and then i believe you will need to translate the docs to base64, 
// add that base 64 to a json payload with the new ticket number you just created and then add a procedure in the python server to 
// modify tickets.


// i grabbed the project id from inpsecting the projects tab on jira.
// the custom field is the digital triage - digital brief field , i found that by doing something similar. Ask Andy pass for info about that
  var data = {
    'project': {'id': 11622},
    'summary': 'DJF-Test-',
    'customfield_13008' : JSON.stringify(contentUpdateBrief),
    'description': 'This ticket was created by the Jira digital form. If anything is wrong please consult Matthew Egan',
    'issuetype': {'name': 'Task'}
  }

  $.ajax({
    url: "http://localhost:5000/ticket",
    type: "POST",
    contentType: "application/json",
    data : JSON.stringify(data),
  }).done(function(response) {
    console.log("this is awesome");

    console.log(response);
  }).fail(function(fail) {
    console.log(fail);
  });
});
