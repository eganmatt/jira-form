//---------- Input Assets ----------//

$(function() {
  i = 1;
  var $container = $('#p_input_container');
  $firstGroup = '<div class="file-input">\
                        <div class="input-file-container upl_drop">\
                          <label for="p_test" class="input-file-trigger">Select a file...\
                            <input type="file" id="p_test" name="p_test_';

 $firstGroupEnd = '"          value=""class="input-file">\
                          </label>\
                        </div>\
                        <span class="remtest btn--remove">Remove</span>\
                </div>'

  $('#addtest').on('click', function() {
    // console.log('index: ' + i ); -- Test if adding asending numbers to name
    i = i + 1;
    var $lastGroup = $container.find('.file-input:last');
    $($firstGroup + i + $firstGroupEnd).insertBefore('#addtest');

  });

  $container.on('click', '.remtest', function(e) {
    if ($('.file-input').length > 1)
      $(this).closest('.file-input').remove();
  }).on('change', '.input-file', function(e) {
    if (!this.files)
      return;

    var $container = $(this).closest('.file-input');
    $container.find(".input-file-trigger").text('File name: ' + this.files[0].name);
    $(".input-file-trigger").parent().addClass('input-file-container-width');
  });
});



//---------- Hide and Show Navigation ----------//


$(document).on('click', '.js-add-row', function() {
  $('table').append($('table').find('tr:last').clone());
});

$(document).on('click', '.js-del-row', function() {
  $('table').find('tr:last').remove();
});

//----------Add field if selected----------//

$(function() {
  $("#drive-date").change(function() {
    if ($("#other").is(":selected")) {
      $("#other-input").show();
    } else {
      $("#other-input").hide();
    }
  }).trigger('change');
});

$(function() {
  $("#drive-date2").change(function() {
    if ($("#other2").is(":selected")) {
      $("#other-input2").show();
    } else {
      $("#other-input2").hide();
    }
  }).trigger('change');
});

//----------Submitted Tick---------//
$(document).ready(function() {
  $letsgetstarted = $('#letsgetstartedtick');
  $signoff = $('#signofftick');
  $copychange = $('#copychangetick');
  $pdfupdate = $('#pdfupdatetick');
  $rateschange = $('#rateschangetick');
  $marketingbanner = $('#marketingbannertick');
  $newpage = $('#newpagetick');
  $smegupdate = $('#smegupdatetick');
  $sitescrape = $('#sitescrapetick');
  $pagedeletion = $('#pagedeletiontick');
  $otherrequest = $('#otherrequesttick');
  $resolutionrequest = $('#resolutionrequesttick');

  $('#letsgetstartedtick, #signofftick, #copychangetick, #pdfupdatetick, #rateschangetick, #marketingbannertick, #newpagetick, #smegupdatetick, #sitescrapetick, #pagedeletiontick, #otherrequesttick, #resolutionrequesttick').hide();
});

//---------- Auto-size Input Area ----------//
$('textarea').on('keydown', function(e){
  if(e.which == 13) {e.preventDefault();}
}).on('input', function(){
  $(this).height(1);
  var totalHeight = $(this).prop('scrollHeight') - parseInt($(this).css('padding-top')) - parseInt($(this).css('padding-bottom'));
  $(this).height(totalHeight);
});


//---------- Add New (clone) ----------//

$('#addnew').on('click', function (){
  var clone = $('#approver').clone();
  var apvbox = $('.apvbox').clone();

  clone.find("input").val("");
  clone.insertAfter('#newapprover');

  $(apvbox).show();
  $('#approver').slideUp();
});

//---------- Add new text areas ----------//
$(function(){
  var count = 0;
  $('#addnew1').click(function(){
    var lastinput = $('<textarea  name="location" id="location'+count+'" class="form-item__control small"></textarea>')
    $(lastinput).insertBefore(this).last();
    count++;
  });
});

$('#newrem1').on('click', function (){
  var inputs = $('.location textarea').length;
  if(inputs == 1){
    
  } else {
    this.previousElementSibling.previousElementSibling.remove();
  }
});

$(function(){
  var count = 0;
  $('#addnew2').click(function(){
    var lastinput = $('<textarea  name="searchterms" id="searchterms'+count+'" class="form-item__control small"></textarea>')
    $(lastinput).insertBefore(this).last();
    count++;
  });
});

$('#newrem2').on('click', function (){
  var inputs = $('.searchterms textarea').length;
  if(inputs == 1){
    
  } else {
    this.previousElementSibling.previousElementSibling.remove();
  }
});

$(function(){
  var count = 0;
  $('#addnew3').click(function(){
    var lastinput = $('<textarea  name="location-request-details" id="locationrequestdetails'+count+'" class="form-item__control small"></textarea>')
    $(lastinput).insertBefore(this).last();
    count++;
  });
});

$('#newrem3').on('click', function (){
  var inputs = $('.locationrequestdetails textarea').length;
  if(inputs == 1){
    
  } else {
    this.previousElementSibling.previousElementSibling.remove();
  }
});

$(function(){
  var count = 0;
  $('#addnew4').click(function(){
    var lastinput = $('<textarea  name="other-request-search-terms" id="otherrequestsearchterms'+count+'" class="form-item__control small"></textarea>')
    $(lastinput).insertBefore(this).last();
    count++;
  });
});

$('#newrem4').on('click', function (){
  var inputs = $('.otherrequestsearchterms textarea').length;
  if(inputs == 1){
    
  } else {
    this.previousElementSibling.previousElementSibling.remove();
  }
});

$(function(){
  var count = 0;
  $('#addnew5').click(function(){
    var lastinput = $(
          '<div class="form-item__select-container"> \
            <select class="form-item__control" name="scrape-domain" id="scrapedomain'+count+'"> \
              <option value="" disabled selected hidden>Select your option</option> \
              <option value="co-operativebank">Co-operative Bank</option> \
              <option value="smile">smile</option> \
              <option value="platform">Platform</option> \
              <option value="britannia">Britannia</option> \
            </select> \
            <span class="form-item__select-arrow"></span> \
          </div>'
        );
    $(lastinput).insertAfter('.scrapedomain');
    count++;
  });
});

$('#newrem5').on('click', function (){
  var inputs = $('.scrapedomain div').length;
  if(inputs == 1){
    
  } else {
    this.previousElementSibling.previousElementSibling.remove();
  }
});

