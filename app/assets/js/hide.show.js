$(document).ready(function() {
  $panels = $('.content-hide-show');
  $panelshow = $('.content-show');
  $content = $('.content-text-hide-show');
  $panels.hide();
  $content.hide();

  $('#drop-menu > li > a').click(function() {
    $panels.hide();
    $content.hide();
    $panelshow.hide();
    $('#' + $(this).attr('aria-controls')).show();
  });

  $('nav > ul > li > a').click(function() {
    $panels.hide();
    $panelshow.hide();
    $('#' + $(this).attr('aria-controls')).show();
  });
});

$(document).ready( function(){
  $('#drop-menu').hide();
  $('.navigation-menu').slideDown();

  $('#drop-trigger').click( function(event){
      
      $('#drop-menu').slideToggle();
      
  });

});
