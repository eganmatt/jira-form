var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var handlebars = require('gulp-compile-handlebars');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var image = require('gulp-image');

/* ## Standard Gulp Task
gulp.task('default', function() {
  // place code for your default task here
});
*/

gulp.task('start', () => {
  return gulp.src('./node_modules/wheelbarrow-forms/dist/**/*.*')
    .pipe(gulp.dest('./public/vendor'));
});

gulp.task('templates', () => {

  const data = require('./locals');

  const options = {
    batch: ['./app/partials','./app/pages'],
  };

  return gulp.src('./app/*.hbs')
    .pipe(handlebars(data, options))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(rename({
      extname: '.html'
    }))
    .pipe(gulp.dest('./public'));
});

gulp.task('webconfig', () => {
  return gulp.src('app/assets/config/web.config')
  .pipe(gulp.dest('./public/'));
});

gulp.task('images', () => {
  return gulp.src('app/assets/img/*')
    .pipe(image())
    .pipe(gulp.dest('./public/image/'));
});

gulp.task('minify-css', () => {
  return gulp.src('app/assets/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./public/css/'));
});
 
gulp.task('scripts', () => {
  return gulp.src('./app/assets/js/*.js')
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./public/js/'));
});

gulp.task('build', ['start','templates', 'scripts', 'minify-css', 'images', 'webconfig'], () => {
});

gulp.task('watch', ['templates', 'scripts', 'minify-css', 'images'], () => {
  gulp.watch('./app/*.hbs', ['templates']);
  gulp.watch('./app/pages/*.hbs', ['templates']);
  gulp.watch('./app/partials/*.hbs', ['templates']);
  gulp.watch('./app/css/*.css', ['templates', 'minify-css']);
  gulp.watch('./app/assets/js/*.js', ['scripts']);
});
