
## Jira Form



---

## Usage

Follow these steps to get installed

1. Clone repository.
2. Navigate to location of clone.
3. Run ' npm install '
4. Run gulp start (This will create a public file and import the wheelbarrow css.)
5. Spin up local server http-server
6. Run ' gulp watch ', this will build the templates out into public along with CSS and JS.

gulp templates - runs template and builds new files and watches css and javascript changes

---

## Contributing

All changes should increment the relevant semantic version value and have the appropriate tag added, example:

`git tag 0.0.2 && git push origin --tags`

1.0.0

1. Minor Changes: Amend far right value (Does not affect anything global)
2. Medium Changes: Amend middle value (adding new components which will effect global but not break any other workspaces)
3. Major Changes: Amend first value (new components which will change the structure causing other workspaces not to work)
